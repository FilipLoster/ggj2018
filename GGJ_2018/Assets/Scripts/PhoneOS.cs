﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;
using UnityEngine.UI;

public class PhoneOS : Singleton<PhoneOS> {
    public enum PhoneState {
        MAIN_MENU,
        NEW_MESSAGE,
        READING_MESSAGE,
        READING_MESSAGE_NEW_ONE_AWAITS,
        REPLYING_TO_MESSAGE,
        PHONE_DED
    }

    #region Bound Game Objects
    [SerializeField]
    protected GameObject _MainMenu;

    [SerializeField]
    protected GameObject _NewMessageFullScreenNotif;

    [SerializeField]
    protected GameObject _ReadingMessageScreen;

    [SerializeField]
    protected GameObject _ReadingMessageScreen_ReplyBox;

    [SerializeField]
    protected GameObject _ReadingMessageScreen_NewMessageNotif;

    [SerializeField]
    private Text _SmsReceiveTextField;

    [SerializeField]
    public Text SmsInputTextField;

    [SerializeField]
    List<Hint> _Hints;

    [SerializeField]
    public Image _Fader;

    [SerializeField]
    public GameObject _Seal;

    [SerializeField]
    public GameObject _BrokenHearth;

    [SerializeField]
    public Image _IconA;

    [SerializeField]
    public Image _IconB;
    #endregion

    PhoneState _State;

    public bool _IsPhoneDed = false;

    List<Msg.NewMsgReceived> _PendingMessages = new List<Msg.NewMsgReceived>();

    Msg.NewMsgReceived _OpenedMessage;

    public PhoneState GetState() {
        if (_IsPhoneDed) return PhoneState.PHONE_DED;

        if (_OpenedMessage == null && _PendingMessages.Count > 0)
            return PhoneState.NEW_MESSAGE;        

        if (_OpenedMessage != null && _PendingMessages.Count > 0)
            return PhoneState.READING_MESSAGE_NEW_ONE_AWAITS;        

        if (_OpenedMessage != null && _PendingMessages.Count == 0 && _OpenedMessage.Answers.Count > 0)
            return PhoneState.REPLYING_TO_MESSAGE;        

        if (_OpenedMessage != null)
            return PhoneState.READING_MESSAGE;

        return PhoneState.MAIN_MENU;
    }

    public void PhoneDed() {
        _IsPhoneDed = true;
        _IconA.gameObject.SetActive(false);
        _IconB.gameObject.SetActive(false);
        UpdateScreen();
    }

    protected void UpdateScreen() {
        _MainMenu.gameObject.SetActive(false);
        _NewMessageFullScreenNotif.gameObject.SetActive(false);
        _ReadingMessageScreen.gameObject.SetActive(false);
        _ReadingMessageScreen_ReplyBox.gameObject.SetActive(false);
        _ReadingMessageScreen_NewMessageNotif.gameObject.SetActive(false);                

        switch (GetState()) {
            case PhoneState.PHONE_DED:
                return;

            case PhoneState.MAIN_MENU:
                _MainMenu.gameObject.SetActive(true);
                break;

            case PhoneState.NEW_MESSAGE:
                _NewMessageFullScreenNotif.gameObject.SetActive(true);                
                break;

            case PhoneState.READING_MESSAGE:
                _ReadingMessageScreen.gameObject.SetActive(true);
                _SmsReceiveTextField.text = _OpenedMessage.Message;
                break;

            case PhoneState.READING_MESSAGE_NEW_ONE_AWAITS:
                _ReadingMessageScreen.gameObject.SetActive(true);
                _ReadingMessageScreen_NewMessageNotif.gameObject.SetActive(true);
                _SmsReceiveTextField.text = _OpenedMessage.Message;
                break;

            case PhoneState.REPLYING_TO_MESSAGE:
                _ReadingMessageScreen.gameObject.SetActive(true);
                _ReadingMessageScreen_ReplyBox.gameObject.SetActive(true);
                _SmsReceiveTextField.text = _OpenedMessage.Message;
                DisplayAnswerHints(_OpenedMessage.Answers);                
                break;
        }
    }

    protected void OnMenuButtonPressed() {
        switch (GetState()) {
            case PhoneState.MAIN_MENU: break;
            
            case PhoneState.NEW_MESSAGE:
                _OpenedMessage = _PendingMessages[0];
                _PendingMessages.RemoveAt(0);
                break;

            case PhoneState.READING_MESSAGE:
                _OpenedMessage = null;
                break;

            case PhoneState.READING_MESSAGE_NEW_ONE_AWAITS:
                _OpenedMessage = _PendingMessages[0];
                _PendingMessages.RemoveAt(0);
                break;

            case PhoneState.REPLYING_TO_MESSAGE:
                SendAnswer(SmsInputTextField.text);                
                break;
        }

        UpdateScreen();
    }

    protected void DisplayAnswerHints(List<StoryManager.StoryAnswer> hints) {
        for (int i = 0; i < _Hints.Count; i++) {
            if (i < hints.Count)
                _Hints[i].SetText(hints[i].Answer);
            else
                _Hints[i].gameObject.SetActive(false);
        }
    }

    protected void Awake() {
        _Seal.gameObject.SetActive(true);
        _BrokenHearth.gameObject.SetActive(false);

        UpdateScreen();
    }

    protected void OnDestroy() {
        TinyTokenManager.Instance.UnregisterAll(this);
    }

    protected void Update() {
        if (PhoneOS.Instance._IsPhoneDed) return;
        if (Input.GetMouseButtonDown(0)) {
            OnMenuButtonPressed();
            KeyboardHandler.Instance.PlaySoundForMouse(0);            
        }

        if (PhoneAntenna.Instance.IsTransmissionPossible && PhoneAntenna.Instance.PendingMessages.Count > 0) {
            _PendingMessages.AddRange(PhoneAntenna.Instance.PendingMessages);
            PhoneAntenna.Instance.PendingMessages.Clear();
            PhoneController.Instance.Clear();

            UpdateScreen();

            for (int i = 0; i < _Hints.Count; i++) {                
                _Hints[i].gameObject.SetActive(false);
            }

            VibrationMotor.Instance.Vibrate();
            KeyboardHandler.Instance.PlayNewMsgSound();
        }
    }

    public bool SendAnswer(String text) {
        if (!PhoneAntenna.Instance.IsTransmissionPossible) return false;
        
        if (!StoryManager.Instance.SendMessage(text)) {            
            return false;
        }

        foreach (Hint h in _Hints) {            
            if (h._Text.text.ToUpper().Trim().CompareTo(text.ToUpper().Trim()) == 0) {
                h.HideSelected();
            } else {
                h.HidePassed();
            }
        }

        SmsInputTextField.text = "";
        PhoneController.Instance.SmsText = "";

        KeyboardHandler.Instance.PlaySendMessageSound();

        _OpenedMessage = null;
        _PendingMessages.Clear();                  

        PhoneAntenna.Instance.SetBatteryBars(
            Math.Max(1, PhoneAntenna.Instance._BatteryValue - 1));

        UpdateScreen();
        
        return true;
    }
}
