﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;

public class StoryManager : Singleton<StoryManager> {
    [Serializable]
    public class StoryAnswer {
        [SerializeField]
        public String Answer = "";

        [SerializeField]
        public int NextStoryPointID = -1;
    }

    [Serializable]
    public class GirlMessage {
        [SerializeField]
        public String Message;

        [SerializeField]
        public int Delay;
    }

    [Serializable]
    public class StoryPoint {
        [SerializeField]
        public String StoryPointID;

        [SerializeField]
        public List<GirlMessage> GirlMessages = new List<GirlMessage>();

        [SerializeField]
        public List<StoryAnswer> Answers = new List<StoryAnswer>();

        [SerializeField]
        public int IrritationDelay;

        [SerializeField]
        public int WhenIgnoredNextStoryPointID = -1;
    }

    #region Properties
    #endregion

    #region Data
    [SerializeField]
    public List<StoryPoint> StoryPoints = new List<StoryPoint>();    
    #endregion

    bool _Started = false;
    StoryPoint _CurrentStoryPoint;
    float _CurrTime;
    int _CurrMessage;

    public void StartCounter() {
        _CurrentStoryPoint = StoryPoints.Count > 0 ? StoryPoints[0] : null;
        _CurrTime = 0;
        _CurrMessage = 0;
        _Started = true;        
    }

    protected void Awake() {
        StartCounter();
    }

    public bool SendMessage(String message) {        
        if (_CurrentStoryPoint == null) return false;
        
        foreach (StoryAnswer ans in _CurrentStoryPoint.Answers) {
            string a = ans.Answer.Trim().ToUpper();
            string b = message.Trim().ToUpper();
                        
            if (String.Compare(a, b, true) == 0) {                
                // Good enough, let's take it!
                _CurrTime = 0;
                _CurrMessage = 0;
                _CurrentStoryPoint = GetNewStoryPoint(ans.NextStoryPointID);
                return true;
            }
        }

        return false;
    }

    protected void OnDestroy() {
        TinyTokenManager.Instance.UnregisterAll(this);
    }

    protected void Update() {
        if (!_Started) return;
        if (_CurrentStoryPoint == null) {
            _Started = false;
            return;
        }

        _CurrTime += Time.deltaTime;

        // regular message
        if (_CurrMessage < _CurrentStoryPoint.GirlMessages.Count()) {            

            GirlMessage message = _CurrentStoryPoint.GirlMessages[_CurrMessage];

            if(_CurrTime >= message.Delay) {
                _CurrTime = 0.0f;
                _CurrMessage++;
                List<StoryManager.StoryAnswer> answers = new List<StoryManager.StoryAnswer>();
                if (_CurrMessage == _CurrentStoryPoint.GirlMessages.Count()) {
                    answers.AddRange(_CurrentStoryPoint.Answers);
                }
                
                TinyMessengerHub.Instance.Publish<Msg.NewMsgReceived>(
                    Msg.NewMsgReceived.Get(message.Message, answers));
            }            
        } 
        // when irritated
        else {
            if (_CurrTime >= _CurrentStoryPoint.IrritationDelay) {
                _CurrTime = 0;
                _CurrMessage = 0;
                _CurrentStoryPoint = GetNewStoryPoint(_CurrentStoryPoint.WhenIgnoredNextStoryPointID);
            }
        }
    }

    private StoryPoint GetNewStoryPoint(int id) {
        if (id < 0) {
            // game over
            if (id == -2) {
                PhoneOS.Instance._Seal.gameObject.SetActive(false);
                PhoneOS.Instance._BrokenHearth.gameObject.SetActive(true);

                PhoneOS.Instance._Fader.DOFade(1.0f, 2.0f).SetDelay(15.0f).OnComplete(() => {
                    //jump to main menu
                    UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                });

                return null;
            }

            // battery ded
            if (id == -3) {
                PhoneOS.Instance.PhoneDed();
                PhoneAntenna.Instance.SetSignalStrength(0);
                PhoneAntenna.Instance.SetBatteryBars(0);

                PhoneOS.Instance._Fader.DOFade(1.0f, 2.0f).SetDelay(3.0f).OnComplete(() => {
                    //jump to true credits
                    UnityEngine.SceneManagement.SceneManager.LoadScene(2);
                });
                return null;
            }
            return null;
        }
        return StoryPoints[id];
    }

    private static int CalcLevenshteinDistance(string a, string b) {
        if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b)) return 9999;

        a = a.ToLower();
        b = b.ToLower();

        int lengthA = a.Length;
        int lengthB = b.Length;
        var distances = new int[lengthA + 1, lengthB + 1];
        for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
        for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

        for (int i = 1; i <= lengthA; i++)
            for (int j = 1; j <= lengthB; j++) {
                int cost = b[j - 1] == a[i - 1] ? 0 : 1;
                distances[i, j] = Math.Min
                    (
                    Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                    distances[i - 1, j - 1] + cost
                    );
            }
        return distances[lengthA, lengthB];
    }
}
