﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Arm : MonoBehaviour {

    [SerializeField]
    protected float _Diff = 0.5f;

    [SerializeField]
    protected float _HandRotForce = 0.5f;

    [SerializeField]
    protected Vector2 _AnchorPos;    

    [SerializeField]
    protected RectTransform _Hand;
    
    protected Vector2 _RAnchorPos;

    RectTransform _ParentRTransf;
    RectTransform _RTransform;
    Vector2 _InitialPos;

    protected void Awake() {
        _ParentRTransf = transform.parent.GetComponent<RectTransform>();
        _RTransform = GetComponent<RectTransform>();
        _InitialPos = _RTransform.anchoredPosition;
    }

    protected void LateUpdate() {
        _RAnchorPos = _RTransform.anchoredPosition + _ParentRTransf.anchoredPosition;
        Vector2 posDiff = _AnchorPos - _RAnchorPos;
        int sign = (int)Mathf.Sign(_AnchorPos.x - _RAnchorPos.x);
        float angle = Math.Abs(Vector2.Angle(Vector2.down, posDiff)) * sign;

        _Hand.rotation = Quaternion.Euler(0, 0, angle * _HandRotForce);

        _RTransform.rotation = Quaternion.Euler(0, 0, angle);
    }
}
