﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Tutorial : MonoBehaviour {
    [SerializeField]
    protected float _DisplayTime;

    [SerializeField]
    protected float _DissapearTime;

    Image _Image;

    protected void Awake() {
        _Image = GetComponent<Image>();
        _Image.DOFade(0.0f, _DisplayTime).SetDelay(_DisplayTime);
    }
}
