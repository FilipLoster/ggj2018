﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class Hint : MonoBehaviour {
    [SerializeField]
    public Text _Text;

    protected RectTransform _RTransform;

    float _SineTimer;

    [Header("Zooming")]
    [SerializeField]
    protected float _MinScale;

    [SerializeField]
    protected float _MaxScale;

    [SerializeField]
    protected float _ScaleSpeed;

    [Header("Rotating")]
    [SerializeField]
    protected float _RotationDelta;

    [SerializeField]
    protected float _RotationSpeed;

    float _RotSpeedDigg;

    float _ScaleSpeedDigg;

    CanvasGroup _Group;

    bool _ForceScale = false;

    public void SetText(String text) {
        if (_RTransform == null) {
            _RTransform = GetComponent<RectTransform>();
            _Group = GetComponent<CanvasGroup>();
        }

        _Text.text = text;
        this.gameObject.SetActive(true);
        _Group.alpha = 0.0f;

        _Group.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        _ForceScale = true;
        DOTween
            .Sequence()
            .InsertCallback(0.0f, () => {
                KeyboardHandler.Instance.PlayAnswersAppearedSound();
            })
            .Insert(0.0f, _Group.DOFade(1.0f, 0.5f))
            .Insert(0.0f, _Group.GetComponent<RectTransform>().DOScale(1.0f, 0.5f))
            .SetDelay(UnityEngine.Random.Range(0.0f, 0.2f))
            .OnComplete(() => {
                _ForceScale = false;
            });
    }

    public void HideSelected() {
        if (_RTransform == null) {
            _RTransform = GetComponent<RectTransform>();
            _Group = GetComponent<CanvasGroup>();
        }

        _ForceScale = true;
        DOTween
            .Sequence()            
            .Insert(0.0f, _Group.DOFade(0.0f, 0.5f))
            .Insert(0.0f, _RTransform.DOScale(2.0f, 0.5f))
            .OnComplete(() => {
                _ForceScale = false;
            });
    }

    public void HidePassed() {
        if (_RTransform == null) {
            _RTransform = GetComponent<RectTransform>();
            _Group = GetComponent<CanvasGroup>();
        }

        _ForceScale = true;
        DOTween
            .Sequence()
            .Insert(0.0f, _Group.DOFade(0.0f, 0.5f))
            .Insert(0.0f, _RTransform.DOScale(0.5f, 0.5f))
            .OnComplete(() => {
                _ForceScale = false;
            });
    }

    protected void Awake() {
        _RTransform = GetComponent<RectTransform>();
        _Group = GetComponent<CanvasGroup>();

        _RotSpeedDigg = UnityEngine.Random.Range(0.0f, 3.14f);
        _ScaleSpeedDigg = UnityEngine.Random.Range(0.0f, 3.14f);
    }

    protected void Update() {
        _SineTimer += Time.deltaTime;

        float scaleValue = (float)(Math.Sin((_SineTimer + _ScaleSpeedDigg) * _ScaleSpeed) + 1.0f) * 0.5f * (_MaxScale - _MinScale) + _MinScale;
        if (!_ForceScale)
            _RTransform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);

        float rotValue = (float)Math.Sin((_SineTimer + _RotSpeedDigg) * _RotationSpeed) * _RotationDelta;
        _RTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, rotValue));
    }
}

