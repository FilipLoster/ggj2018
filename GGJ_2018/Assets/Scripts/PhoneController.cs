﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PhoneController : Singleton<PhoneController> {

	#region const value
	private const char NO_INPUT = '_';
	private const char SPACE = ' ';
	private const float TIME_OUT = 0.75f;
	#endregion

    [SerializeField]
    protected Vector2 _AnchorPoint = new Vector2(0, 300);

    [SerializeField]
    protected float _MaxDistance = 100;

    [SerializeField]
    protected float _SpringMax = 10;

    [SerializeField]
    protected float _SpringYMax = 500;

    [SerializeField]
    protected float _MaxAnchorXDelta = 200;

    public Vector2 PhonePosition {
        get { 
            if(_Transform == null)
                _Transform = this.GetComponent<RectTransform>();
            return _Transform.anchoredPosition; 
        }
    }

	#region TMP fields
	private string _SmsText;
	private char[] _1Fields;
	private char[] _2Fields;
	private char[] _3Fields;
	private char[] _4Fields;
	private char[] _5Fields;
	private char[] _6Fields;
	private char[] _7Fields;
	private char[] _8Fields;

	private int _LastButtonPressed;
	private int _ArrayPtr;
	private char _SingleLetter;

	#endregion
	private RectTransform _Transform;
	private float _TimeOutCounter;

	public string SmsText{
		get{
			return _SmsText;
		}
		set{
			_SmsText = value;
			UpdateText();
		}
	}

	protected override void  Awake(){
		base.Awake();

        _Transform = this.GetComponent<RectTransform>();

		_1Fields = new char[3]{'A', 'B', 'C'};
		_2Fields = new char[3]{'D', 'E', 'F'};
		_3Fields = new char[3]{'G', 'H', 'I'};
		_4Fields = new char[3]{'J', 'K', 'L'};
		_5Fields = new char[3]{'M', 'N', 'O'};
		_6Fields = new char[4]{'P', 'Q', 'R', 'S'};
		_7Fields = new char[3]{'T', 'U', 'V'};
		_8Fields = new char[4]{'W', 'X', 'Y', 'Z'};

        Clear();
	}

	// Use this for initialization
	void Start () {
		UpdateText();
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePosition();

        if (PhoneOS.Instance._IsPhoneDed) return;

		if(Input.GetMouseButtonDown(1)){
			EraseLastLetter();
            KeyboardHandler.Instance.PlaySoundForMouse(1);
		}

		if(Input.GetButtonDown("1")){
			ProcessButton(1);
			KeyboardHandler.Instance.PlaySoundForKey(1);
		}

		if(Input.GetButtonDown("2")){
			ProcessButton(2);
			KeyboardHandler.Instance.PlaySoundForKey(2);
		}

		if(Input.GetButtonDown("3")){
			ProcessButton(3);
			KeyboardHandler.Instance.PlaySoundForKey(3);
		}

		if(Input.GetButtonDown("4")){
			ProcessButton(4);
			KeyboardHandler.Instance.PlaySoundForKey(4);
		}

		if(Input.GetButtonDown("5")){
			ProcessButton(5);
			KeyboardHandler.Instance.PlaySoundForKey(5);
		}

		if(Input.GetButtonDown("6")){
			ProcessButton(6);
			KeyboardHandler.Instance.PlaySoundForKey(6);
		}

		if(Input.GetButtonDown("7")){
			ProcessButton(7);
			KeyboardHandler.Instance.PlaySoundForKey(7);
		}

		if(Input.GetButtonDown("8")){
			ProcessButton(8);
			KeyboardHandler.Instance.PlaySoundForKey(8);
		}

		if(Input.GetButtonDown("9")){
			ProcessButton(9);
			KeyboardHandler.Instance.PlaySoundForKey(9);
		}

		_TimeOutCounter += Time.deltaTime;

		if(_TimeOutCounter >= TIME_OUT)
			PrintLetter();        
	}

    protected void UpdatePosition() {
        Vector2 newPosition = Input.mousePosition;
        newPosition = new Vector2(
            newPosition.x,
            Mathf.Clamp(newPosition.y, _AnchorPoint.y, _SpringYMax));

        float nPosXAnchored = newPosition.x - _AnchorPoint.x;
        if (Mathf.Abs(nPosXAnchored) > _MaxAnchorXDelta) {
            nPosXAnchored = _MaxAnchorXDelta * Mathf.Sign(nPosXAnchored);
            newPosition.x = nPosXAnchored + _AnchorPoint.x;
        }

        Vector2 diff = newPosition - _AnchorPoint;

        if (diff.magnitude > _MaxDistance) {
            Vector2 nDiff = diff.normalized;
            float diffVal = Mathf.Min(diff.magnitude - _MaxDistance, _SpringMax);
            float stretch = diffVal / _SpringMax;
            float powValue = 1.0f - 0.5f * stretch;

            VibrationMotor.Instance.StretchVibrations = stretch * 0.1f;

            newPosition =
                _AnchorPoint +
                nDiff * (_MaxDistance + powValue * diffVal);
        } else {
            VibrationMotor.Instance.StretchVibrations = 0.0f;
        }

        _Transform.anchoredPosition = newPosition;
    }

	private void EraseLastLetter(){
        if (PhoneOS.Instance.GetState() != PhoneOS.PhoneState.REPLYING_TO_MESSAGE) return;

		PrintLetter();
		if(_SmsText.Length > 0){
			_SmsText = _SmsText.Remove(_SmsText.Length - 1, 1);
			_ArrayPtr = 0;
			_LastButtonPressed = NO_INPUT;

			UpdateText();
		}

	}

	private void ProcessButton(int buttonPressed){
        if (PhoneOS.Instance.GetState() != PhoneOS.PhoneState.REPLYING_TO_MESSAGE) return;

		if(_LastButtonPressed != buttonPressed || buttonPressed == 9){
			_LastButtonPressed = buttonPressed;
			PrintLetter();

			if(buttonPressed == 9){
				_SingleLetter = SPACE;
				PrintLetter();
			}
		}

		switch(buttonPressed){
			case 1:
				_SingleLetter = _1Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _1Fields.Length) ? 0 : _ArrayPtr + 1;
				break;
			
			case 2:
				_SingleLetter = _2Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _2Fields.Length) ? 0 : _ArrayPtr + 1;
				break;
			
			case 3:
				_SingleLetter = _3Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _3Fields.Length) ? 0 : _ArrayPtr + 1;
				break;

			case 4:
				_SingleLetter = _4Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _4Fields.Length) ? 0 : _ArrayPtr + 1;
				break;
			
			case 5:
				_SingleLetter = _5Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _5Fields.Length) ? 0 : _ArrayPtr + 1;
				break;

			case 6:
				_SingleLetter = _6Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _6Fields.Length) ? 0 : _ArrayPtr + 1;
				break;

			case 7:
				_SingleLetter = _7Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _7Fields.Length) ? 0 : _ArrayPtr + 1;
				break;

			case 8:
				_SingleLetter = _8Fields[_ArrayPtr];
				_ArrayPtr = (_ArrayPtr + 1 >= _8Fields.Length) ? 0 : _ArrayPtr + 1;
				break;
		}
		
		UpdateText();
		_TimeOutCounter = 0;
	}

	private void PrintLetter(){
		if(_SingleLetter != NO_INPUT){
			_SmsText = _SmsText + _SingleLetter;
			_SingleLetter = NO_INPUT;
			_ArrayPtr = 0;
		}
	}

	private void UpdateText(){
        PhoneOS.Instance.SmsInputTextField.text = _SmsText + ((_SingleLetter == NO_INPUT) ? SPACE : _SingleLetter);
	}

    public void Clear() {
        _ArrayPtr = 0;
        _LastButtonPressed = 0;
        _SingleLetter = SPACE;
        _SmsText = "";

        UpdateText();
    }
}
