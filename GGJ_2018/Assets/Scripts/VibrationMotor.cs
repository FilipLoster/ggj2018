﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class VibrationMotor : Singleton<VibrationMotor> {
    
    float _MaxAmplitude = 0.0f;

    float _VibrationSpeed = 50.0f;

    float _Time = 0.0f;

    public float StretchVibrations = 0.0f;

    RectTransform _RTransform;

    protected void Awake() {
        _RTransform = GetComponent<RectTransform>();        
    }

    protected void OnDestroy() {        
    }

    public void Vibrate() {
        DOTween.Kill(this);
        _MaxAmplitude = 3.0f;
        DOTween.To(x => _MaxAmplitude = x, 3.0f, 0.0f, 0.5f);
    }

    protected void Update() {
        _Time += Time.deltaTime;
        float rotValue = (float)Math.Sin(_Time * _VibrationSpeed) * Math.Max(StretchVibrations, _MaxAmplitude);

        _RTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, rotValue));
    }
}
