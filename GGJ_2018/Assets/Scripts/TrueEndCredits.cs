﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TrueEndCredits : MonoBehaviour {

	[SerializeField]
	private Image _Panel;

	[SerializeField]
	private Sprite[] _PagesToDisplay;

	[SerializeField]
	private float _Length = 10;

	private float _Time = 0;
	private int _ArrayPtr = 0;
	private float _TimeStep = 0.75f;

	void Start(){
		AnimatePage();
	}

	void AnimatePage(){
		_Panel.sprite = _PagesToDisplay[_ArrayPtr];

		_Panel.DOFade(1, _TimeStep)
			.OnComplete(() =>{
				_Panel.DOFade(0, _TimeStep * 2)
				.SetDelay(_Length/_PagesToDisplay.Length)
				.OnComplete(() => {
					_ArrayPtr += 1;

					if(_ArrayPtr < _PagesToDisplay.Length)
						AnimatePage();
					else
						UnityEngine.SceneManagement.SceneManager.LoadScene(0);
				});	

				
			});
	}
	
}
