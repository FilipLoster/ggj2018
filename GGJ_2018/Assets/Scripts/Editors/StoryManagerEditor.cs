﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StoryManager))]
public class StoryManagerEditor : Editor {
    int expandedTextOption = -1;

    public override void OnInspectorGUI() {
        StoryManager manager = (StoryManager)target;
        
        manager.StoryPoints.EnsureSize(
            EditorGUILayout.IntSlider(manager.StoryPoints.Count(), 0, 20),
            (x) => {
                return new StoryManager.StoryPoint();
            });

        int counter = 0;
        foreach (StoryManager.StoryPoint point in manager.StoryPoints) {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUILayout.BeginHorizontal();
            point.StoryPointID = EditorGUILayout.TextField("ID", point.StoryPointID);

            if (counter != expandedTextOption) {
                if (GUILayout.Button("Expand")) {
                    expandedTextOption = counter;
                }
            }
            EditorGUILayout.EndHorizontal();

            if (counter == expandedTextOption) {                            
                EditorGUILayout.LabelField("Girl Message");            

                point.GirlMessages.EnsureSize(
                    EditorGUILayout.IntSlider(point.GirlMessages.Count(), 0, 9), 
                    (x) => { return new StoryManager.GirlMessage(); });
                for (int i = 0; i < point.GirlMessages.Count; i++) {
                    EditorGUILayout.BeginHorizontal();
                    point.GirlMessages[i].Message = EditorGUILayout.TextField(point.GirlMessages[i].Message, GUILayout.MinWidth(200));
                    point.GirlMessages[i].Delay = EditorGUILayout.IntSlider(point.GirlMessages[i].Delay, 0, 120);
                    EditorGUILayout.EndHorizontal();
                }                

                EditorGUILayout.LabelField("Answers");
                point.Answers.EnsureSize(
                    EditorGUILayout.IntSlider(point.Answers.Count(), 0, 3),
                    (x) => { return new StoryManager.StoryAnswer(); });
                for (int i = 0; i < point.Answers.Count; i++) {
                    EditorGUILayout.BeginHorizontal();
                    point.Answers[i].Answer = EditorGUILayout.TextField(point.Answers[i].Answer);
                    point.Answers[i].NextStoryPointID = showStoryPointPicker("Next Story Point", manager, point.Answers[i].NextStoryPointID);
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.LabelField("Irritation");
                point.IrritationDelay = EditorGUILayout.IntSlider("Irritation delay (sec)", point.IrritationDelay, 0, 120);
                point.WhenIgnoredNextStoryPointID = showStoryPointPicker("SP when irritated", manager, point.WhenIgnoredNextStoryPointID);

            }
            EditorGUILayout.EndVertical();
            counter++;
        }

        if (!EditorApplication.isPlaying) {
            EditorUtility.SetDirty(manager);
            EditorApplication.MarkSceneDirty();
        }
    }

    private int showStoryPointPicker(String label, StoryManager manager, int selectedIndex) {
        List<String> options = new List<String>();

        options.Add("BATTERY DED");
        options.Add("GAME OVER");
        options.Add("NONE");

        foreach (StoryManager.StoryPoint point in manager.StoryPoints)
            options.Add(point.StoryPointID);

        return EditorGUILayout.Popup(label, selectedIndex + 3, options.ToArray()) - 3;
    }
}

#endif