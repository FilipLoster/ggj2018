﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class SignalDebugger : MonoBehaviour {

    [SerializeField]
    RectTransform _RTransform;

    protected void Update() {
        _RTransform.anchoredPosition = PhoneAntenna.Instance._SignalPosition;
    }
}

