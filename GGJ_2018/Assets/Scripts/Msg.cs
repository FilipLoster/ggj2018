﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Msg {
    public class NewMsgReceived : TinyMessenger.ITinyMessage {        

        public String Message;
        public List<StoryManager.StoryAnswer> Answers = new List<StoryManager.StoryAnswer>();        
        
        public static NewMsgReceived Get(
                String message,
                List<StoryManager.StoryAnswer> answers) {            
            NewMsgReceived msg = new NewMsgReceived();

            msg.Message = message;
            msg.Answers.AddRange(answers);

            return msg;
        }

        public object Sender {
            get { return null; }
        }        
    }

    public class SignalStrengthChanged : TinyMessenger.ITinyMessage {

        public int Strength;

        public static SignalStrengthChanged Get(int strength) {
            SignalStrengthChanged msg = new SignalStrengthChanged();

            msg.Strength = strength;

            return msg;
        }

        public object Sender {
            get { return null; }
        }
    }
}