﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardHandler : Singleton<KeyboardHandler> {

	[SerializeField]
	private KeyboardButton[] _Buttons;

	[SerializeField]
	private KeyboardButton[] _MouseButtons;

	[SerializeField]
	private AudioSource _AudioSource;

    [SerializeField]
    private AudioClip _VibrationSound;

    [SerializeField]
    private AudioClip _MsgSentSound;

    [SerializeField]
    private AudioClip _NewMessageSound;

	void Awake(){
		_AudioSource = this.GetComponent<AudioSource>();
	}

	public void PlaySoundForKey(int key){
		//_AudioSource.Stop();
		_AudioSource.PlayOneShot(_Buttons[key -1].Pressed());
	}

    public void PlayNewMsgSound() {
        _AudioSource.PlayOneShot(_VibrationSound);
        _AudioSource.PlayOneShot(_NewMessageSound);
    }

    public void PlaySendMessageSound() {
        _AudioSource.PlayOneShot(_MsgSentSound);
    }

    public void PlayAnswersAppearedSound() {
        _AudioSource.PlayOneShot(_MsgSentSound);
    }

	public void PlaySoundForMouse(int button){
		_AudioSource.PlayOneShot(_MouseButtons[button].Pressed());
	}

}
