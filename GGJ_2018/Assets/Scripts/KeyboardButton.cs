﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class KeyboardButton : MonoBehaviour {

	[SerializeField]
	private Image _HighlightState;

	[SerializeField]
	private Image _NormalState;

	[SerializeField]
	private AudioClip _SoundToPlay;

	void Awake(){
		_NormalState.color = Color.white;
	}

	public AudioClip Pressed(){
		_HighlightState.DOFade(1, 0.15f)
			.OnComplete(() => {
				_HighlightState.DOFade(0, 0.15f).SetDelay(0.05f);
			});

		return _SoundToPlay;
	}
}
