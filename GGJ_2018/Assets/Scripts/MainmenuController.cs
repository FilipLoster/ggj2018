﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class MainmenuController : MonoBehaviour {

	[SerializeField]
	private RectTransform _Logo;

	[SerializeField]
	private Button _PlayBtn;

	[SerializeField]
	private Button _CreditsBtn;


	[SerializeField]
	private GameObject _MainMenuView;

	[SerializeField]
	private GameObject _MainCreditsView;

	[SerializeField]
	private GameObject _MusicPlayerPrefab;

	internal static AudioSource MusicPlayer = null;
	
	void Start(){
		if(MainmenuController.MusicPlayer == null){
			MainmenuController.MusicPlayer = GameObject.Instantiate(_MusicPlayerPrefab).GetComponent<AudioSource>();
			MainmenuController.MusicPlayer.loop = true;
			MainmenuController.MusicPlayer.Play();
			Screen.SetResolution((int)(1024 * 1.8f),(int)( 768 * 1.8f), false);
		}

		_Logo.DOScale(Vector3.one, 0.75f).SetDelay(0.2f);
		_PlayBtn.image.DOFade(1, 0.75f).SetDelay(0.8f).OnComplete(() => {
			_PlayBtn.interactable = true;
		});
		_CreditsBtn.image.DOFade(1, 0.75f).SetDelay(1.2f).OnComplete(() => {
			_CreditsBtn.interactable = true;
		});
		

	}

	public void OnStarClick(){
		SceneManager.LoadScene(1);
	}

	public void OnCreditsClicked(){
		_MainMenuView.SetActive(!(_MainMenuView.activeInHierarchy));
		_MainCreditsView.SetActive(!(_MainCreditsView.activeInHierarchy));
	}

}
