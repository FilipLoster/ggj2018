﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class GridLayoutGroupExtensions {
    public static void ResizeToMatchContentHorizontally(this GridLayoutGroup group, int rowsCount = 1) {
        int childCount = (int)Math.Ceiling((float)group.transform.childCount / (float)rowsCount);

        RectTransform containerTransforms = group.GetComponent<RectTransform>();
        containerTransforms.sizeDelta = new Vector2(
            group.cellSize.x * childCount +
            group.spacing.x * (childCount - 1) +
            group.padding.left + group.padding.right,
            containerTransforms.sizeDelta.y);
    }

    public static void ResizeToMatchContentVertically(this GridLayoutGroup group, int columnsCount = 1) {
        int childCount = (int)Math.Ceiling((float)group.transform.childCount / (float)columnsCount);

        RectTransform containerTransforms = group.GetComponent<RectTransform>();
        containerTransforms.sizeDelta = new Vector2(            
            containerTransforms.sizeDelta.x,
            group.cellSize.y * childCount +
            group.spacing.y * (childCount - 1) + 
            group.padding.top + group.padding.bottom);
    }
}
