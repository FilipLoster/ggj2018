﻿using System;

public class ReflectionUtils {
    public static T GetAttributeFromEnum<T, E>(E enm) 
                where T : Attribute
                where E : struct, IConvertible {
            var componentType = typeof(E);
            var memInfo = componentType.GetMember(enm.ToString());

            object[] arr = memInfo[0].GetCustomAttributes(typeof(T), true);

            if (arr.Length == 0) {

                return null;
            }

            return (T)arr[0];        
    }
}