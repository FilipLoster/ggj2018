﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Reflection;

public static class GameObjectExtensions {
    private static string[] _SortingLayers;

    public static void RemoveAllChildren(this GameObject obj) {
        int childs = obj.transform.childCount;
        for (int i = childs; i > 0; i--)
            GameObject.DestroyImmediate(obj.transform.GetChild(i - 1).gameObject);                
    }

    public static void RemoveAllChildrenNotImmediate(this GameObject obj) {
        int childs = obj.transform.childCount;
        for (int i = childs; i > 0; i--)
            GameObject.Destroy(obj.transform.GetChild(i - 1).gameObject);
    } 

    // Source: http://answers.unity3d.com/questions/530178/how-to-get-a-component-from-an-object-and-add-it-t.html
    public static T AddComponentCopy<T>(this GameObject go, T component) where T : Component {
        Component newComponent = go.AddComponent<T>();
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        Type type = component.GetType();
        PropertyInfo[] pinfos = typeof(T).GetProperties(flags);

        foreach (var pinfo in pinfos) {
            if (pinfo.CanWrite) {
                try {
                    pinfo.SetValue(newComponent, pinfo.GetValue(component, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos) {
            finfo.SetValue(newComponent, finfo.GetValue(component));
        }

        return newComponent as T;
    }

    public static void ChangeSortingLayerRecursive(this GameObject root, int layer) {
        root.layer = layer;

        for (int i = 0; i < root.transform.childCount; i++ )
            ChangeSortingLayerRecursive(root.transform.GetChild(i).gameObject, layer);
    }
}

