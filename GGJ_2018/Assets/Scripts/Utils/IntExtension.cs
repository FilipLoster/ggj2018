﻿using UnityEngine;
using System.Collections;

public static class FloatExtension {

    public static float ClosestPow2Up(this float value) {
        int pow = 2;
        while(pow < value) {
            pow *= 2;
        }
        return pow;
    }
}
