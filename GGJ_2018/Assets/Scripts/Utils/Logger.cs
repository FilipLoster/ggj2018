﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Logger : Singleton<Logger> {
    public enum LogType {
        ASSETS_LOADING,
        EVENT_CARD_PICKING,
        MERCHANT_CARD_PICKING,
        QUEST_STATE
    }

    [SerializeField]
    protected bool _AssetsLoading;
    [SerializeField]
    protected bool _EventCardPicking;
    [SerializeField]
    protected bool _MerchantCardPicking;
    [SerializeField]
    protected bool _QuestState;

    Dictionary<LogType, String> _LogGroup = new Dictionary<LogType, string>();
    Dictionary<LogType, bool> _GroupLoggedAnything = new Dictionary<LogType, bool>();

    public void Log(LogType type, String logMessage) {
        if (!CanLog(type)) return;

        if(_LogGroup.ContainsKey(type)) {
            _LogGroup[type] = String.Format(
                "{0}\n{1}", 
                _LogGroup[type], 
                logMessage);
            _GroupLoggedAnything[type] = true;
        } else {
            Debug.Log(String.Format("{0}: {1}", type.ToString(), logMessage));
        }
        
    }

    public void BeginLogGroup(LogType type, String title) {
        if (!CanLog(type)) return;

        _LogGroup.Remove(type);
        _GroupLoggedAnything.Remove(type);
        _LogGroup[type]= String.Format("***** {0} *****", title);
    }

    public void EndLogGroup(LogType type) {
        bool succes = true;
        if (!CanLog(type)) succes = false;
        if (!_LogGroup.ContainsKey(type)) succes = false;
        if (!_GroupLoggedAnything.ContainsKey(type)) succes = false;

        if (succes) {
            Log(type, "************");
            Debug.Log(_LogGroup[type]);
        }

        _LogGroup.Remove(type);
        _GroupLoggedAnything.Remove(type);
    }

    protected bool CanLog(LogType type) {
        switch(type) {
            case LogType.ASSETS_LOADING:
                return _AssetsLoading;
            case LogType.EVENT_CARD_PICKING:
                return _EventCardPicking;
            case LogType.MERCHANT_CARD_PICKING:
                return _MerchantCardPicking;
            case LogType.QUEST_STATE:
                return _QuestState;
            default:
                return true;
        }
    }
}

