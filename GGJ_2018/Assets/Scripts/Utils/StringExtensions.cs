﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public static class StringExtensions {
    public static String Clamp(this String data, int newSize) {
        String result = data;
        if(data.Count() > newSize) {
            result = String.Format("{0}...", data.Substring(0, newSize));
        }
        return result;
    }    
}