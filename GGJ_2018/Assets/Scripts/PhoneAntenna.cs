﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PhoneAntenna : Singleton<PhoneAntenna> {

    [Header("Signal")]
    [SerializeField]
    protected float _SignalRadius;

    [SerializeField]
    protected Vector2 _SignalRangeRectMin;

    [SerializeField]
    protected Vector2 _SignalRangeRectMax;

    [SerializeField]
    protected float _DiffPerSignalBar;

    [SerializeField]
    protected float _MovementSpeed = 0.5f;

    public Vector2 _SignalPosition;

    protected Vector2 _TargetSignalPosition;

    [Header("Bound Game Objects")]
    [SerializeField]
    List<RectTransform> _SignalBars;

    [SerializeField]
    List<RectTransform> _BatteryBars;

    public List<Msg.NewMsgReceived> PendingMessages = new List<Msg.NewMsgReceived>();

    int _SignalStrength;

    public int _BatteryValue;

    public bool IsTransmissionPossible {
        get { return _SignalStrength > 2; }
    }

    public void SetSignalStrength(int strength) {
        if (PhoneOS.Instance._IsPhoneDed) strength = 0;

        _SignalStrength = strength;
        for (int i = 0; i < _SignalBars.Count; i++)
            _SignalBars[i].gameObject.SetActive(i < strength);        
    }

    public void SetBatteryBars(int value) {
        if (PhoneOS.Instance._IsPhoneDed) value = 0;

        _BatteryValue = value;

        for (int i = 0; i < _BatteryBars.Count; i++)
            _BatteryBars[i].gameObject.SetActive(i < value);
    }

    protected void Awake() {
        SetSignalStrength(5);
        SetBatteryBars(5);

        TinyTokenManager.Instance.Register<Msg.SignalStrengthChanged>(this, (m) => {
            SetSignalStrength(m.Strength);
        });
        TinyTokenManager.Instance.Register<Msg.NewMsgReceived>(this, (m) => {
            PendingMessages.Add(m);
        });

        _SignalPosition = new Vector2(
            UnityEngine.Random.Range(_SignalRangeRectMin.x, _SignalRangeRectMax.x),
            UnityEngine.Random.Range(_SignalRangeRectMin.y, _SignalRangeRectMax.y));
        RandNewSignalPositionTarget();        
    }

    protected void OnEnable() {
        CalcSignalStrength();
    }

    protected void CalcSignalStrength() {
        Vector2 diff = PhoneController.Instance.PhonePosition - _SignalPosition;
        int diffDiscrete = (int)Math.Floor(diff.magnitude / _DiffPerSignalBar);
        int sigStrength = 5;

        int len = 1;
        while (diffDiscrete > 0) {
            diffDiscrete -= len;
            len++;
            sigStrength--;
        }

        SetSignalStrength(sigStrength);
    }

    protected void OnDestroy() {
        TinyTokenManager.Instance.UnregisterAll(this);
    }

    protected void RandNewSignalPositionTarget() {
        while ((_SignalPosition - _TargetSignalPosition).magnitude < 5)
            _TargetSignalPosition = new Vector2(
                UnityEngine.Random.Range(_SignalRangeRectMin.x, _SignalRangeRectMax.x),
                UnityEngine.Random.Range(_SignalRangeRectMin.y, _SignalRangeRectMax.y));        
    }

    protected void Update() {
        if (PhoneOS.Instance._IsPhoneDed) return;

        RandNewSignalPositionTarget();

        Vector2 diff = _TargetSignalPosition - _SignalPosition;
        diff = diff.normalized;        
        _SignalPosition += diff * Time.deltaTime * _MovementSpeed;

        CalcSignalStrength();
    }
}

