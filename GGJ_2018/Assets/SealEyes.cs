﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SealEyes : MonoBehaviour {

    [SerializeField]
    Sprite[] _SealSprites;

    [SerializeField]
    float _TimePerSprite;

    Image _Image;    

    int currSealSprite = 0;

    float sealSpriteTimer = 0;

	// Use this for initialization
	void Start () {
        _Image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        sealSpriteTimer += Time.deltaTime;
        while (sealSpriteTimer > _TimePerSprite) {
            sealSpriteTimer -= _TimePerSprite;
            currSealSprite = (currSealSprite + 1) % _SealSprites.Length;

            _Image.sprite = _SealSprites[currSealSprite];
        }
	}
}
